import React from 'react';

import SocialIcons from "../social-icons/SocialIcons";

import logoImg from '../../asset/img/logo.svg';

import './Header.scss';

const Header = () => {

    return (
        <div className='container'>
            <div className='header relative'>
                <img className='header__logo' src={logoImg} alt='logo' />
                <div className='header__social-icons'>
                    <SocialIcons />
                </div>
            </div>
        </div>
    )
};

export default Header;