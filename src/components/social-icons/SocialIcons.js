import React from 'react';

import vkImg from '../../asset/img/social-icons/vk.svg';
import twitterImg from '../../asset/img/social-icons/twitter.svg';
import telegramImg from '../../asset/img/social-icons/telegram.svg';
import instagramImg from '../../asset/img/social-icons/instagram.svg';

import './SocialIcons.scss';

const SocialIcons = () => {
    return (
        <ul className='social-icons'>

            <li className='social-icons__item'>
                <a className='social-icons__link' href='https://instagram.com/cryptcontent?igshid=1qngszllks4qz' target='_blank'>
                    <img src={instagramImg} alt='instagram' />
                </a>
            </li>

            <li className='social-icons__item'>
                <a className='social-icons__link' href='https://vk.com/public202984728' target='_blank'>
                    <img src={vkImg} alt='vk' />
                </a>
            </li>

            <li className='social-icons__item'>
                <a className='social-icons__link' href='https://t.me/CryptCContent' target='_blank'>
                    <img src={telegramImg} alt='telegram' />
                </a>
            </li>

            <li className='social-icons__item'>
                <a className='social-icons__link' href='https://twitter.com/ContentCrypt' target='_blank'>
                    <img src={twitterImg} alt='twitter' />
                </a>
            </li>

        </ul>
    )
}

export default SocialIcons;