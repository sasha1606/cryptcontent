import React, { useEffect, useState } from 'react';

import starImg from "../../asset/img/home/home-hero/star.svg";
import starsImg from "../../asset/img/home/home-hero/stars.svg";

import './Timer.scss';

const Timer = () => {

    const [seconds, setSeconds] = useState('00');
    const [minutes, setMinutes] = useState('00');
    const [hours, setHours] = useState('00');
    const [days, setDays] = useState('00');

    useEffect(() => {
        startTimer('2021/03/22 11:00:00 UTC');
    }, []);

    const startTimer = (date) => {
        const countDownDate = new Date(date).getTime();

        // timerID ???
        setInterval(() => {

            // Get today's date and time
            const now = new Date().getTime();

            // Find the distance between now and the count down date
            const distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            const days = Math.floor(distance / (1000 * 60 * 60 * 24));
            const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((distance % (1000 * 60)) / 1000);

            const daysWithZero = distance < 0 ? '00' : addZero(days);
            const hoursWithZero = distance < 0 ? '00' : addZero(hours);
            const minutesWithZero = distance < 0 ? '00' : addZero(minutes);
            const secondsWithZero = distance < 0 ? '00' : addZero(seconds);

            setSeconds(secondsWithZero);
            setMinutes(minutesWithZero);
            setHours(hoursWithZero);
            setDays(daysWithZero);

        }, 1000);
    };

    const addZero = (item) => {
        return String(item).length === 1 ? `0${item}` : item;
    };

    return (
        <div className='timer'>
            <div className='timer__line timer__line_days relative'>
                <img className='timer__days-star' src={starImg} alt='' />
                <div className='timer__item timer__item_days'>
                    <div className='timer__info'>
                        <span className='timer__number'> { days } </span>
                        <span className='timer__name'> days </span>
                    </div>
                </div>
            </div>

            <div className='timer__line timer__line_hours relative'>
                <img className='timer__hours-stars' src={starsImg} alt='' />
                <div className='timer__item timer__item_hours'>
                    <div className='timer__info'>
                        <span className='timer__number'> { hours } </span>
                        <span className='timer__name'> hours </span>
                    </div>
                </div>
            </div>

            <div className='timer__line timer__line_minutes'>
                <div className='timer__item timer__item_minutes'>
                    <div className='timer__info'>
                        <span className='timer__number'> { minutes } </span>
                        <span className='timer__name'> min. </span>
                    </div>
                </div>

                <div className='timer__item timer__item_seconds'>
                    <div className='timer__info'>
                        <span className='timer__number'> { seconds } </span>
                        <span className='timer__name'> sec. </span>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default Timer;

