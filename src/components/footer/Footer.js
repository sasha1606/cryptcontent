import React from 'react';

import logoImg from '../../asset/img/logo_white.svg';

import './Footer.scss';

const Footer = () => {

    return (
        <div className='footer-wrap'>
            <div className='container'>
                <div className='footer'>
                    <img className='footer__logo' src={logoImg} alt='logo' />
                </div>
            </div>
        </div>
    )
};

export default Footer;