import React from 'react';

import './HomeRoadmap.scss';

const HomeRoadmap = () => {

    return (
        <div className='lines-bg'>
            <div className='container'>
                <div className='home-roadmap'>
                    <h3 className='title home-roadmap__title'>
                        Roadmap
                    </h3>
                    <ul className='home-roadmap-list'>
                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                25.08.2020
                            </p>
                            <p className='home-roadmap-list__description'>
                                Creating and developing the idea of an anonymous
                                blockchain project Crypt Content 
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                05.09.2020
                            </p>
                            <p className='home-roadmap-list__description'>
                                Personnel search and writing a work plan
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                18.02.2021 
                            </p>
                            <p className='home-roadmap-list__description'>
                                Creation and visual design of the platform website  
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                18.03.2021
                            </p>
                            <p className='home-roadmap-list__description'>
                                Start of CONT token distribution and launch of referral reward system
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                20.03.2021
                            </p>
                            <p className='home-roadmap-list__description'>
                                Launch preparations for IEO 
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                22.03.2021
                            </p>
                            <p className='home-roadmap-list__description'>
                                Pre-purchase of tokens and launch of trading
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                11.07.2021
                            </p>
                            <p className='home-roadmap-list__description'>
                                Launching the beta version of the Crypt Content platform 
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                05.09.2021
                            </p>
                            <p className='home-roadmap-list__description'>
                                Adding tools for anonymous transmission of digital content. 
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                22.09.2021
                            </p>
                            <p className='home-roadmap-list__description'>
                                Integration of the CONT token into the platform ecosystem
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                17.10.2021
                            </p>
                            <p className='home-roadmap-list__description'>
                                Launch of the beta version of the Crypt Content platform
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                09.11.2021-30.12.2021
                            </p>
                            <p className='home-roadmap-list__description'>
                                Conducting an advertising campaign 
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                15.01.2022
                            </p>
                            <p className='home-roadmap-list__description'>
                                Searching for platform vulnerabilities and fix bugs 
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                07.02.2022
                            </p>
                            <p className='home-roadmap-list__description'>
                                Collection and analysis feedback from users
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                03.03.2022
                            </p>
                            <p className='home-roadmap-list__description'>
                                Creating a mobile app
                            </p>
                        </li>

                        <li className='home-roadmap-list__item'>
                            <p className='marker marker_sm home-roadmap-list__date'>
                                11.05.2022
                            </p>
                            <p className='home-roadmap-list__description'>
                                Updating the platform website and mobile app
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
};

export default HomeRoadmap;