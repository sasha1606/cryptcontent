import React from 'react';

import homeWhatImg from '../../../asset/img/home/home-what/home-what__main-img.svg';

import './HomeWhat.scss';

const HomeWhat = () => {

    return (
        <div className='container'>
            <div className='home-what'>
                <div className='home-what-first-block'>
                    <div className='home-what-first-block__content'>
                        <h3 className='title home-what-first-block__title'>
                            What is the CRYPTCONTENT?
                        </h3>
                        <p className='text text_mb'>
                            CRYPTCONTENT is an anonymous blockchain-based 
                            platform based on masternodes, whose main 
                            purpose, anonymous storage and transmission 
                            of data in encrypted form.
                        </p>
                        <p className='text text_mb'>
                            Transfer, store, send untraceable 
                            encrypted messages and files over 
                            the Internet. The system features 
                            an open-source technology wallet for 
                            an extra layer of security.
                        </p>
                        <p className='text'>
                            All this is achieved through a 
                            combination of specially designed 
                            hardware, software and Blockchain technology.
                        </p>
                    </div>
                    <div className='home-what-first-block__img-wrap'>
                       <img className='home-what-first-block__img' src={homeWhatImg} alt='' />
                    </div>
                </div>

                <div className='home-what-second-block'>
                    <div className='home-what-second-block__vision'>
                        <h3 className='marker title title_main-color home-what-second-block__title'>
                            Our vision
                        </h3>
                        <div className='home-what-second-block__indent'>
                            <p className='text'>
                                No amount of cryptography, protocol 
                                enhancement, and technical optimization 
                                will help a project with an unstable 
                                and outdated ideology.
                                CRYPTCONTENT isn’t just a 
                                blockchain, but an open source 
                                project aimed at changing the way we 
                                store and transmit data to protect privacy!
                            </p>
                        </div>
                    </div>
                    <div className='home-what-second-block__technologies'>
                        <h3 className='title marker title_main-color home-what-second-block__title'>
                            Safety Technologies
                        </h3>
                        <div className='home-what-second-block__indent'>
                            <p className='text text_mb'>
                                The CRYPTCONTENT blockchain is what adds a 
                                layer of privacy on top of all the 
                                features offered by special CONTECH 
                                hardware and the CONTECH app for sending 
                                and receiving encrypted messages.
                            </p>
                            <p className='text'>
                                At the same time, it creates a paid 
                                content management system that allows 
                                users to get rewarded for their uploaded 
                                content to the CRYPTCONTENT network and masternodes 
                                to get paid for their hosting services.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HomeWhat;