import React from 'react';

import homeAboutImg from '../../../asset/img/home/home-about/home-about__main-img.svg';

import './HomeAbout.scss';

const HomeAbout = () => {

    return (
        <div className='container container__mob-full-width'>
            <div className='home-about'>
                <div className='home-about-first-block'>
                    <div className='home-about-first-block__item'>
                        <h3 className='title home-about__title'>
                            Related equipment
                        </h3>
                        <p className='text text_mb'>
                            Maximum security can only be achieved by combining 
                            hardware and software, which is why our team has 
                            developed special open source CONTECH hardware to 
                            further enhance security.
                            Secure digital storage for cryptocurrency payments, 
                            secure messaging, password management, file encryption and more.
                        </p>
                        <p className='text'>
                            CONTECH is specially designed to work and interact 
                            with the CRYPTCONTENT blockchain to maximize your security and privacy.
                        </p>
                    </div>
                    <div className='home-about-first-block__item'>
                        <img src={homeAboutImg} alt='' />
                    </div>
                </div>

                <div className='home-about-second-block'>
                    <h3 className='title home-about__title'>
                        Content monetization
                    </h3>
                    <div className='home-about-second-block__inner'>
                        <div className='home-about-second-block__item'>
                            
                            <p className='text text_mb'>
                                CONT is a service coin that is required to transfer 
                                anonymous encrypted data in the CRYPTCONTENT blockchain, 
                                as well as to monetize and distribute content.
                            </p>
                            <p className='text'>
                                Content creators are given opportunities to get paid 
                                directly for every download, views and purchases. 
                            </p>
                        </div>
                        <div className='home-about-second-block__item'>
                            <p className='text strong text_mb'>
                                Just upload content, request payment and get CONT!
                            </p>
                            <p className='text'>
                                CRYPTCONTENT Blockchain monetizes 
                                content distribution by allowing content providers 
                                to get paid for their contributions.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default HomeAbout;