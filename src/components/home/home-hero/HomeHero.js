import React from 'react';

import whitePaperLink from '../../../asset/file/white-paper.pdf';
import Timer from '../../timer/Timer';

import './HomeHero.scss';

const HomeHero = () => {

    return (
        <div className='container'>
            <div className='home-hero'>
                <div className='home-hero__content'>
                    <h1 className='home-hero__title'>
                        Anonymous exchange and storage of data on the Blockchain 
                    </h1>
                    <p className='home-hero__description'>
                        The CRYPTCONTENT blockchain is what adds a layer of privacy 
                        on top of all the features offered by special CONTECH 
                        hardware and the CONTECH app for sending and receiving encrypted messages.
                    </p>
                    <a className='btn display-only-desktop' href={whitePaperLink} target='_blank'>
                        WhitePapper
                    </a>
                </div>
                <div className='home-hero__timer'>
                    <Timer />
                </div>
                <div className='display-only-mob'>
                    <a className='btn' href={whitePaperLink} target='_blank'>
                        WhitePapper
                    </a>
                </div>
            </div>
        </div>
    )
};

export default HomeHero;