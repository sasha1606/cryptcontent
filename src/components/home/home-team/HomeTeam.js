import React from 'react';

import teamAvatar1 from '../../../asset/img/home/home-team/1.png';
import teamAvatar2 from '../../../asset/img/home/home-team/2.png';
import teamAvatar3 from '../../../asset/img/home/home-team/3.png';
import teamAvatar4 from '../../../asset/img/home/home-team/4.png';
import teamAvatar5 from '../../../asset/img/home/home-team/5.png';

import './HomeTeam.scss';

const HomeTeam = () => {

    return (
        <div className='container'>
            <div className='home-team'>
                <h3 className='title home-team__title'>Team</h3>
                <div className='home-team-user'>
                    <div className='home-team-user__item'>
                        <img className='home-team-user__avatart' src={teamAvatar1} alt='' />
                        <p className='home-team-user__fio'>
                            Christopher  Chandler
                        </p>
                        <p className='home-team-user__position'>
                            Webmaster
                        </p>
                    </div>

                    <div className='home-team-user__item'>
                        <img className='home-team-user__avatart' src={teamAvatar2} alt='' />
                        <p className='home-team-user__fio'>
                            Walter  Merritt
                        </p>
                        <p className='home-team-user__position'>
                            Top Programmer
                        </p>
                    </div>

                    <div className='home-team-user__item'>
                        <img className='home-team-user__avatart' src={teamAvatar3} alt='' />
                        <p className='home-team-user__fio'>
                            Ronald McKinney
                        </p>
                        <p className='home-team-user__position'>
                            The founder of the project
                        </p>
                    </div>

                    <div className='home-team-user__item'>
                        <img className='home-team-user__avatart' src={teamAvatar4} alt='' />
                        <p className='home-team-user__fio'>
                            Magnus Hines
                        </p>
                        <p className='home-team-user__position'>
                            IT Assistant
                        </p>
                    </div>

                    <div className='home-team-user__item'>
                        <img className='home-team-user__avatart' src={teamAvatar5} alt='' />
                        <p className='home-team-user__fio'>
                            Mark Beasley
                        </p>
                        <p className='home-team-user__position'>
                            Software Specialist
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default HomeTeam;