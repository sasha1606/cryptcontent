import React from 'react';

import './HomeToken.scss';

const HomeToken = () => {

    return (
        <div className='container container__mob-full-width'>
            <div className='home-token'>
                <h3 className='title title_main-color home-token__title'>Circulation of CONT tokens:</h3>
                <ul className='home-token-advantages'>
                    <li className='marker home-token-advantages__item'>
                        Anonymous transfers
                    </li>

                    <li className='marker home-token-advantages__item'>
                        Payment for hosting support   
                    </li>

                    <li className='marker home-token-advantages__item'>
                        Payments for downloading published content
                    </li>

                    <li className='marker home-token-advantages__item'>
                        Payments for view 
                    </li>
                </ul>
                <div className='home-token-distribution'>
                    <h3 className='title home-token-distribution__title'>
                        Distribution 
                        <br />
                        of tokens
                    </h3>
                    <div className='home-token-distribution-grid'>
                        <div className='home-token-distribution-grid__line home-token-distribution-grid__line_space-between'>
                            <div className='home-token-distribution-grid__item home-token-distribution-grid__item_main-color home-token-distribution-grid__item_price'>
                                <p className='home-token-distribution-grid__name'>
                                    The price of IEO: 
                                </p>
                                <p className='home-token-distribution-grid__price'>
                                    0,00000010 BTC
                                </p>
                            </div>
                            {/* this empty block need for mob version */}
                            <div className='home-token-distribution-grid__item home-token-distribution-grid__item_empty display-only-mob' />
                            <div className='home-token-distribution-grid__item home-token-distribution-grid__item_empty display-only-mob' />

                            <div className='home-token-distribution-grid__item'>
                                <p className='home-token-distribution-grid__name'>
                                    Tokens allocated for 
                                    Staking Pool : 
                                </p>
                                <p className='home-token-distribution-grid__price'>
                                    60 000 000 
                                </p>
                            </div>
                        </div>

                        <div className='home-token-distribution-grid__line home-token-distribution-grid__line_center'>
                            <div className='home-token-distribution-grid__item'>
                                <p className='home-token-distribution-grid__name'>
                                    Amount of tokens 
                                    allocated for 
                                    IEO: 
                                </p>
                                <p className='home-token-distribution-grid__price'>
                                    100 000 000
                                </p>
                            </div>
                            {/* this empty block need for mob version */}
                            <div className='home-token-distribution-grid__item home-token-distribution-grid__item_empty display-only-mob' />
                        </div>

                        <div className='home-token-distribution-grid__line home-token-distribution-grid__line_space-between'>
                            {/* this empty block need for mob version */}
                            <div className='home-token-distribution-grid__item home-token-distribution-grid__item_empty display-only-mob' />

                            <div className='home-token-distribution-grid__item'>
                                <p className='home-token-distribution-grid__name'>
                                   Tokens allocated for Airdrop
                                </p>
                                <p className='home-token-distribution-grid__price'>
                                    25 000 000
                                </p>
                                <p className='home-token-distribution-grid__info'>
                                    Will be frozen till 25.04.21
                                </p>
                            </div>

                            <div className='home-token-distribution-grid__item'>
                                <p className='home-token-distribution-grid__name'>
                                    Tokens allocated to the team
                                </p>
                                <p className='home-token-distribution-grid__price'>
                                    5 000 000
                                </p>
                                <p className='home-token-distribution-grid__info'>
                                    Will be frozen till 01.09.21
                                </p>
                            </div>
                        </div>

                        <div className='home-token-distribution-grid__line home-token-distribution-grid__line_center'>
                            {/* this empty block need for mob version */}
                            <div className='home-token-distribution-grid__item home-token-distribution-grid__item_empty display-only-mob' />

                            <div className='home-token-distribution-grid__item'>
                                <p className='home-token-distribution-grid__name'>
                                    Marketing
                                </p>
                                <p className='home-token-distribution-grid__price'>
                                    10 000 000
                                </p>
                                <p className='home-token-distribution-grid__info'>
                                    Competition and tournaments
                                </p>
                            </div>
                        </div>

                        <div className='home-token-distribution-grid__line home-token-distribution-grid__line_end'>
                            <div className='home-token-distribution-grid__item home-token-distribution-grid__item_total home-token-distribution-grid__item_main-color'>
                                <p className='home-token-distribution-grid__name'>
                                    Total supply
                                </p>
                                <p className='home-token-distribution-grid__price'>
                                    200 000 000
                                </p>
                            </div>
                            {/* this empty block need for mob version */}
                            <div className='home-token-distribution-grid__item home-token-distribution-grid__item_empty display-only-mob' />
                        </div>

                    </div>
                </div>
           </div>
        </div>
    )
};

export default HomeToken;