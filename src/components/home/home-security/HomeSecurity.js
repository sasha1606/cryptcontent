import React from 'react';

import homeSecurityImg from '../../../asset/img/home/home-security/home-security__main-img.svg';

import './HomeSecurity.scss';

const HomeSecurity = () => {

    return (
        <div className='container container__mob-full-width'>
            <div className='home-security'>
                <h2 className='title home-security__title'>
                    Internet security
                </h2>
                <div className='home-security-wrap'>
                    <div className='home-security-wrap__item'>
                        <p className='text text_mb'>
                            Everyone who uses the Internet today, 
                            wants to keep their actions in secret, 
                            and for this resorted to the use of special 
                            equipment or separate software, however it doesn’t 
                            exclude the possibility of the remainder of digital traces.
                        </p>
                        <div className='display-only-mob home-security-wrap__text-mb'>
                            <p className='text text_mb'>
                                Everyone who uses the Internet today, wants to keep 
                                their actions in secret, and for this resorted to 
                                the use of special equipment or separate software, 
                                however it doesn’t exclude the possibility of the 
                                remainder of digital traces.
                                Your Internet Service Provider or anyone else 
                                interested in your personal information may 
                                connect your files and messages to your payments 
                                and other Internet activities.
                            </p>
                            <p className='text'>
                                <span className='strong'>CRYPTCONTENT</span> Blockchain is designed by our team to solve this difficult problem.
                                With our technology, your messages and data are not only encrypted, but also untraceable.
                            </p>
                        </div>
                        <img className='home-security-wrap__img' src={homeSecurityImg} alt='' />
                    </div>
                    <div className='home-security-wrap__item display-only-desktop'>
                        <p className='text text_mb'>
                            Everyone who uses the Internet today, wants to keep 
                            their actions in secret, and for this resorted to 
                            the use of special equipment or separate software, 
                            however it doesn’t exclude the possibility of the 
                            remainder of digital traces.
                            Your Internet Service Provider or anyone else 
                            interested in your personal information may 
                            connect your files and messages to your payments 
                            and other Internet activities.
                        </p>
                        <p className='text'>
                            <span className='strong'>CRYPTCONTENT</span> Blockchain is designed by our team to solve this difficult problem.
                            With our technology, your messages and data are not only encrypted, but also untraceable.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default HomeSecurity;