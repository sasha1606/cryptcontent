import React from 'react';

import Header from "../../components/header/Header";
import HomeHero from "../../components/home/home-hero/HomeHero";
import HomeSecurity from "../../components/home/home-security/HomeSecurity";
import HomeWhat from '../../components/home/home-what/HomeWhat';
import HomeAbout from '../../components/home/home-about/HomeAbout';
import HomeTeam from '../../components/home/home-team/HomeTeam';
import Footer from "../../components/footer/Footer";
import HomeRoadmap from '../../components/home/home-roadmap/HomeRoadmap';
import HomeToken from '../../components/home/home-token/HomeToken';

import './Home.scss';


const Home = () => {
    return (
        <>
            <div className='lines-bg'>
                <Header />
                <HomeHero />
                <HomeSecurity />
            </div>
            <HomeWhat />
            <HomeAbout />
            <HomeTeam />
            <HomeToken />
            <HomeRoadmap />
            <Footer />
        </>
    )
};

export default Home;

